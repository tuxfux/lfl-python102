#!/usr/bin/python
# -*- coding: utf-8 -*-
import MySQLdb as mdb
conn = mdb.connect('localhost','user102','user102','lfl102')
cur = conn.cursor()
cur.execute("create table student (name varchar(10),gender varchar(6))")
conn.close()


'''
ysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| lfl102             |
+--------------------+
2 rows in set (0.00 sec)

mysql> use lfl102;
Database changed
mysql> show tables;
Empty set (0.00 sec)

## after running create.py

mysql> show tables;
+------------------+
| Tables_in_lfl102 |
+------------------+
| student          |
+------------------+
1 row in set (0.00 sec)

mysql> desc student;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| name   | varchar(10) | YES  |     | NULL    |       |
| gender | varchar(6)  | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
2 rows in set (0.01 sec)

mysql>




'''