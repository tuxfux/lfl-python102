#!/usr/bin/env python
# -*- coding: utf-8 -*-
# break - which will break me out of a loop.I will still be in the program.
# exit/sys.exit -> take you out of the program.
# continue
'''
In [7]: import sys

In [8]: sys.exit?
Docstring:
exit([status])

Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).
Type:      builtin_function_or_method

'''

import sys

number = 7 # written in my palm
#test = True

game = input("do you want to play the game: y/n - ")

if game == 'n':
    sys.exit()


#while test:  # when your condition is True
while True:
    num = int(input("please guess the number:"))
    
    if num > number:
        print ("Buddy !! you guessed a larger number")
    elif num < number:
        print ("Buddy !! you guessed a smaller number")
    else:
        print ("congo !! you guessed the right number")
        #test = False
        break
        
print ("Please come back !!!")

'''
outer
  inner
     break

'''
        

