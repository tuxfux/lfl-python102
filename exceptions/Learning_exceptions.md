Exception/Error Handling
Exception handling is done is pre-productions.
exception kick you out of the program.
## try..except..else..finally

```python
## case I
num1 = int(input("please enter your num1:"))
num2 = int(input("pleaes enter your num2:"))
result = num1/num2
print ("result of numbers:{}".format(result))
```

    please enter your num1:ten
    


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-3-43e6767a673e> in <module>
          1 ## case I
    ----> 2 num1 = int(input("please enter your num1:"))
          3 num2 = int(input("pleaes enter your num2:"))
          4 result = num1/num2
          5 print ("result of numbers:{}".format(result))
    

    ValueError: invalid literal for int() with base 10: 'ten'



```python
## case II
num1 = int(input("please enter your num1:"))
num2 = int(input("pleaes enter your num2:"))
result = num1/num2
print ("result of numbers:{}".format(result))
```

    please enter your num1:10
    pleaes enter your num2:0
    


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-4-28064fcad290> in <module>
          2 num1 = int(input("please enter your num1:"))
          3 num2 = int(input("pleaes enter your num2:"))
    ----> 4 result = num1/num2
          5 print ("result of numbers:{}".format(result))
    

    ZeroDivisionError: division by zero

## what are the various exception we support - 2.x

In [4]: import exceptions as e

In [5]: print dir(e)
['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BufferError', 'BytesWarning', 'DeprecationWarning', 'EOFError', 'EnvironmentError', 'Exception', 'FloatingPointError', 'FutureWarning', 'GeneratorExit', 'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'NameError', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'ReferenceError', 'RuntimeError', 'RuntimeWarning', 'StandardError', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'WindowsError', 'ZeroDivisionError', '__doc__', '__name__', '__package__']
## what are the various exceptions we support - 3.x

In [3]: import builtins as exceptions

In [4]: print (dir(exceptions))
['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FileExistsError', 'FileNotFoundError', 'FloatingPointError', 'FutureWarning', 'GeneratorExit', 'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'WindowsError', 'ZeroDivisionError', '__IPYTHON__', '__build_class__', '__debug__', '__doc__', '__import__', '__loader__', '__name__', '__package__', '__spec__', 'abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'display', 'divmod', 'enumerate', 'eval', 'exec', 'filter', 'float', 'format', 'frozenset', 'get_ipython', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']

In [5]:                           # blocks
# try - for block of code which can lead to exception.
# except - what needs to be done with exception raised in try block.
# else - if there is no error in try block go to the else block.

```python
## how to deal with exception
# except is capturing a lot of exception.
# case I
try:
    num1 = int(input("please enter your num1:"))
    num2 = int(input("pleaes enter your num2:"))
    result = num1/num2
except:
    print ("Please enter numbers.Make sure denominator is not zero.")
else:
    print ("result of numbers:{}".format(result))
```

    please enter your num1:10
    pleaes enter your num2:2
    result of numbers:5.0
    


```python
# case II
try:
    num1 = int(input("please enter your num1:"))
    num2 = int(input("pleaes enter your num2:"))
    result = num1/num2
except:
    print ("Please enter numbers.Make sure denominator is not zero.")
else:
    print ("result of numbers:{}".format(result))
```

    please enter your num1:10
    pleaes enter your num2:0
    Please enter numbers.Make sure denominator is not zero.
    


```python
## how to capture specific exceptions
try:
    num1 = int(input("please enter your num1:"))
    num2 = int(input("pleaes enter your num2:"))
    result = num1/num2
except (ValueError,ZeroDivisionError):
    print ("Please enter numbers.Make sure denominator is not zero.")
else:
    print ("result of numbers:{}".format(result))
```

    please enter your num1:a
    Please enter numbers.Make sure denominator is not zero.
    


```python
## granular
try:
    num1 = int(input("please enter your num1:"))
    num2 = int(input("pleaes enter your num2:"))
    result = num1/num2
except ValueError:
    print ("Please enter numbers.")
except ZeroDivisionError:
    print ("Make sure denominator is not zero")
else:
    print ("result of numbers:{}".format(result))
```

    please enter your num1:10
    pleaes enter your num2:0
    Make sure denominator is not zero
    
## finally
# Flipkart(walmart) big billion day.
# website went down - Autoscaling didnt fit.

# customers
# live transaction.
# not getting the acknowledgement.

## snapdeal(Tata) clubing alibaba(china)
# snapdeal(India) - UFT8
- developers(hyd) - qwerty
- testing(chennai) - qwerty

china - snapdeal (hu and mandrak)
snapdeal ( latin - Uk and US and India)

```python
## finally
## case I: i will enter values which are valid - try..else..finally
## case II: i will enter invalid values handled by exception - try..except..finally
## case III: i will enter invalid values not handled by exception - try..finally..bombed out of the program

try:
    num1 = int(input("please enter your num1:"))
    num2 = int(input("pleaes enter your num2:"))
    result = num1/num2
except ValueError:
    print ("Please enter numbers.")
else:
    print ("result of numbers:{}".format(result))
finally:
    print ("All is well")
    # database.close
    # file.close
    # socket.close
```

    please enter your num1:10
    pleaes enter your num2:0
    All is well
    


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-1-c1fa262b008c> in <module>
          7     num1 = int(input("please enter your num1:"))
          8     num2 = int(input("pleaes enter your num2:"))
    ----> 9     result = num1/num2
         10 except ValueError:
         11     print ("Please enter numbers.")
    

    ZeroDivisionError: division by zero

# exception - custom - OOP
# raise     - raising a exception - OOP