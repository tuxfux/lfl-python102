Programming
* Modular programming.
* OOP/object programming.

OOP:
* APIE
A - Abstraction
P - Polymorphism
I - Inheritance
E - Encapsulation


A - Abstraction

 - -> variables/data
 + -> functions/methods

car      -> blueprint/class
 - wheels
 - engine
 - color
 + acclerate
 + break
 + clutch
 
Models  -> replicas/objects of class/instances
------
toyoto
bmw
honda
lamborhini
subaru
 
 
## bank account

 - -> variables/data
 + -> functions/methods

Account     -> blueprint/class
  - balance=0
  + deposit
  + withdraw
  - interest - 4%
  - Account type 
  
Client       -> instance/objects
 - santosh
 - milad
 - sunil

## build a villa

villa (class/blueprint)

rowhouse   - instance
  - santosh
  - milad
  - sunil
  
## P - Polymorphism

poly - many
morphism - forms

+ - it works for both numbers and strings.

In [1]: a = 1

In [2]: b = 2

In [3]: a + b
Out[3]: 3

In [4]: c = "linux"

In [5]: d = " rocks"

In [6]: c + d
Out[6]: 'linux rocks'

In [7]:

# polymorpic

Milad
  - son
  - brother
  - student
  - tutor
  
## bankaccount
interest
 - savings bank account
 - home loan interest
 - study loan interest
 - car loan interest
 - current bank account
   
## I - Inheritance - reducing the code

Father
 - right hand
 - black eyes
 - brown hair
 - Artist
Mother
 - left hand
 - brown eyes
 - blonde hair
 - Architect

+ son(Father,Mother)
 - right hand
 - black eyes
 - blonde hair
 - teacher
+ daughter(Mother,Father)
 - brown eyes
 - blonde hair
 - right hand
 - professor

## E - Encapsulation

data hiding - public,private,protected (No)
accessing data using functions - setters and getters

ex:
milad - cashier
sunil - friend

Account
 - balance
 + deposit
 + withdraw
 
example: pen drive

notes:
https://radek.io/2011/07/21/private-protected-and-public-in-python/
