#!/usr/bin/python
# -*- coding: utf-8 -*-
## setter and getter
# requirments from ten.py
# 1. I dont want people to access the lenght directly.
# 2. I dont need my lenght to be negative

class Area:
    def __init__(self,length=1):
        self.__length = length     # private
        
    def compute_area(self):
        my_area = self.__length * self.__length
        return (my_area)

    ## implement the getter method
    @property
    def length(self):
        print ("Implementing the @property method")
        return (self.__length)
    
    ## implement the setter method
    @length.setter
    def length(self,value):
        print ("Implementing the @setter method")
        if value < 0:
            raise ValueError("Length cannot be less than zero.")
        self.__length = value
        
## Main
A = Area(10)
print (A.length)
# modify the lenght
A.length = 20
print (A.length)
print (A.compute_area())

