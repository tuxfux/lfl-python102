#!/usr/bin/python
# -*- coding: utf-8 -*-
## https://radek.io/2011/07/21/private-protected-and-public-in-python/

## public

#class Cup:
#    def __init__(self):
#        self.color = None   # public
#        self.content = None # public
#
#    def fill(self, beverage):
#        self.content = beverage
#
#    def empty(self):
#        self.content = None
#        
#    def display(self):
#        print (self.content)
#        
#Green = Cup()
#Green.color = "Green"
#Green.content = "tea"
#print (Green.color)
#print (Green.content)

## protected

#class Cup:
#    def __init__(self):
#        self.color = None    # public
#        self._content = None # protected variable
#
#    def fill(self, beverage):
#        self._content = beverage
#
#    def empty(self):
#        self._content = None
#    
#    def display(self):
#        print (self._content)
#        
#        
### main
#Red = Cup()
#Red.display()
#Red.fill("coffee")
#Red.display()
#Red.empty()
#Red.display()
#
#Red._content = "Coffee"
#Red.display()

## private


class Cup:
    def __init__(self, color):
        self._color = color    # protected variable
        self.__content = None  # private variable

    def fill(self, beverage):
        self.__content = beverage

    def empty(self):
        self.__content = None
        
    def display(self):
        print (self.__content)
        
# Main
yellow = Cup("yellow")
yellow.fill("horlicks")
yellow.display()
yellow.empty()
yellow.display()

yellow.__content = "complain"
yellow.display()

## Name Mangling
print (yellow._Cup__content)
yellow._cup__content = "horlicks"
print (yellow._Cup__content)
