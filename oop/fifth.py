#!/usr/bin/python
## constructor,defined our Account class
# -*- coding: utf-8 -*-

class Account:
    def __init__(self,initial=0):  # special method,construtors
        self.balance = initial
    def my_deposit(self,amount):
        self.balance = self.balance + amount
    def  my_withdraw(self,amount):
        self.balance = self.balance - amount
    def my_balance(self):
        return ("my balance is {}".format(self.balance))
    
# sunil
sunil = Account(1000) # __init__ is the first method which is touched.
print (sunil.my_balance())
sunil.my_deposit(5000)
print (sunil.my_balance())
sunil.my_withdraw(2000)
print (sunil.my_balance())

# Milad
milad = Account()
print (milad.my_balance())
milad.my_deposit(10000)
print (milad.my_balance())
milad.my_withdraw(1000)
print (milad.my_balance())

## 
print (milad.balance)
print (sunil.balance)


