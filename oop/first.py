#!/usr/bin/python
# -*- coding: utf-8 -*-

balance = 0

def my_deposit(amount):
    global balance
    balance =  balance + amount
    return balance

def my_withdraw(amount):
    global balance
    balance = balance - amount
    return balance

## MAIN
# milad
print ("Initial balance of milad - {}".format(balance))
my_deposit(1000)
my_withdraw(200)
my_withdraw(200)
print ("Initial balance of milad - {}".format(balance))
# sunil
print ("Initial balance of sunil - {}".format(balance))
my_deposit(1000)
my_withdraw(200)
my_withdraw(200)
print ("Initial balance of sunil - {}".format(balance))

