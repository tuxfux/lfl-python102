#!/usr/bin/python
# understanding class and instance variables.
# -*- coding: utf-8 -*-

# class/blueprint
# class is a child of object class.
#class Account(object):
class Account:
    balance = 0 # data
    def my_balance():
        print ("my balance is {}".format(balance))
        
        
# instance/object
print (Account)       # <class '__main__.Account'>
print (type(Account)) # <class 'type'>
sunil = Account()     # defining your instance.     
print (sunil)           # <__main__.Account object/instance at 0x000002788B919A90)
print (type(sunil))     # <class '__main__.Account'>/instance of account class
print (type(Account())) # <class '__main__.Account'>/instance of account class
print (Account())       # <__main__.Account object/instance at 0x000002788B919EF0>

## Access the data
print (dir(sunil)) # object linked to sunil - balance,my_balance
print (sunil.balance) # accessing balance as instance/object sunil.
                      # instance data/variable
print (Account.balance) # class variable
                        # variable i can access over a class
sunil.balance = 100
print (sunil.balance)   # I made modification to balance variabel for instance sunil.

print (Account.balance) # class variable and any modification to it will modify all
                        # instance variables
                        
# Account variable
Account.balance = 200
print (sunil.balance)  # 100
print (Account.balance) # 200

# Milad
milad = Account()
print (milad.balance)  # 200

