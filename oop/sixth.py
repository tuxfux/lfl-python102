#!/usr/bin/python
# -*- coding: utf-8 -*-

# generic account
class Account:             # parent class
    def __init__(self,initial=0):  # special method,construtors
        self.balance = initial
    def my_deposit(self,amount):
        self.balance = self.balance + amount
    def  my_withdraw(self,amount):
        self.balance = self.balance - amount
    def my_balance(self):
        return ("my balance is {}".format(self.balance))

## child class will inherit all method implicitly due to inheritance.
## it cannot inherit __init__ explicitly.
class MinBalAccount(Account): # child class
    def __init__(self,initial=1000):
        Account.__init__(self,initial)
    def my_withdraw(self,amount):
        if self.balance - amount < 1000:
            print ("Buddy!! time to call your daddy - {}".format(self.balance - 1000))
        else:
            Account.my_withdraw(self,amount)
            
## method with same name - local function will be given higher precedence.
      

## sunil - salaried
## overdraft feature
sunil = Account()
sunil.my_deposit(5000)
sunil.my_withdraw(4000)
sunil.my_withdraw(4000)
print (sunil.my_balance()) # -3000

## Milad - student
milad = MinBalAccount()
print (milad.my_balance())
milad.my_deposit(5000)
print (milad.my_balance())
milad.my_withdraw(2000)
milad.my_withdraw(2500)
print (milad.my_balance())
milad.my_withdraw(2500)
milad.my_withdraw(300)
print (milad.my_balance())
