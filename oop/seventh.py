#!/usr/bin/python
# -*- coding: utf-8 -*-

#In [10]: from builtins import SyntaxError
#
#In [11]: raise SyntaxError("hey there how about cleaning your glasses")
#Traceback (most recent call last):
#
#  File "C:\Users\tuxfux\Anaconda3\lib\site-packages\IPython\core\interactiveshell.py", line 3325, in run_code
#    exec(code_obj, self.user_global_ns, self.user_ns)
#
#  File "<ipython-input-11-7a6c40b7086d>", line 1, in <module>
#    raise SyntaxError("hey there how about cleaning your glasses")
#
#  File "<string>", line unknown
#SyntaxError: hey there how about cleaning your glasses


## exception handling

# Exception - parent class
class InvalidAgeException(Exception):
    def __init__(self,age):
        self.age = age

def validate_age(age):
    if age >= 18:
        print ("You are right age.Welcome!!")
    else:
        raise InvalidAgeException(age)
    
## Main
age = int(input("please enter your age:"))

try:
    validate_age(age)
except Exception as e:
    print ("Buddy you are still a kiddo - {}".format(e.age))
else:
    pass
finally:
    pass


