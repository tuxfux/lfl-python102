#!/usr/bin/python
# -*- coding: utf-8 -*-

class Area:
    def __init__(self,length=1):
        self.length = length
        
    def compute_area(self):
        my_area = self.length * self.length
        return (my_area)
    
    
## Main
A = Area(10)
print (A.length)
# modify the lenght
A.length = 20
print (A.length)
print (A.compute_area())

