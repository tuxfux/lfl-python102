#!/usr/bin/python
# -*- coding: utf-8 -*-
# dunders or magic methods
# https://rszalski.github.io/magicmethods/
# polymorphism -> + (int,str,RationalNumber)
# operator overloading

class RationalNumber:
    def __init__(self,numerator,denominator=1):
        self.n = numerator
        self.d = denominator
        
    def __add__(self,other):
        ## for the object not part of rational number class
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)
        ## taking care of adding two rational numbers.
        n = self.n * other.d + self.d * other.n # (1*3 + 2*1)
        d = self.d * other.d                    # 3*2 
        return RationalNumber(n,d) # instance of RationalNumber class
    
    def __str__(self):
        return ("{}/{}".format(self.n,self.d))
        
    __repr__ = __str__   


## Main
### __add__,__str__ feature in the RationalNumber class
a = RationalNumber(1,2) # a.n = 1,a.d = 2
b = RationalNumber(1,3) # b.n = 1,b.d = 3
print (a + b) # a.__add__(b)
### __add__,__str__ as part of instance class
a = 1 # <class 'int'>
b = 2 # <class 'int'>
print (a + b) # 3
### one value from RationalNumber and one from int class
a = RationalNumber(1,2)  # 1/2
b = 3                    # <class 'int'> => 3/1
print (a + b)  # a.__add__(b) # a.__add__(other)







## notes
#In [1]: 1 + 2
#Out[1]: 3
#
#In [2]: "linux" + "rocks"
#Out[2]: 'linuxrocks'
#
#In [3]: 1/2 + 1/3
#Out[3]: 0.8333333333333333
#
#In [4]: 1/2
#Out[4]: 0.5
#
#In [5]: 1/3
#Out[5]: 0.3333333333333333
#
#In [6]: # 1/2 + 1/3 = (1*3 + 2*1)/3*2 = 5/6
#
#In [7]: 'linux' + 5
#---------------------------------------------------------------------------
#TypeError                                 Traceback (most recent call last)
#<ipython-input-7-f35aa4c68c12> in <module>
#----> 1 'linux' + 5
#
#TypeError: can only concatenate str (not "int") to str

#In [13]: a = 1
#
#In [14]: b = 2
#
#In [15]: a.__add__(b)
#Out[15]: 3
#
#In [16]: c = "linux"
#
#In [17]: d = " rocks"
#
#In [18]: c.__add__(d)
#Out[18]: 'linux rocks'

#In [8]: isinstance?
#Signature: isinstance(obj, class_or_tuple, /)
#Docstring:
#Return whether an object is an instance of a class or of a subclass thereof.
#
#A tuple, as in ``isinstance(x, (A, B, ...))``, may be given as the target to
#check against. This is equivalent to ``isinstance(x, A) or isinstance(x, B)
#or ...`` etc.
#Type:      builtin_function_or_method
#
#In [9]: isinstance(a,int)
#Out[9]: True
#
#In [10]: isinstance(a,str)
#Out[10]: False

