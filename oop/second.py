#!/usr/bin/python

def Account():
     return {'balance':0}

def my_deposit(account,amount):
    account['balance'] =  account['balance'] + amount
    return account['balance']

def my_withdraw(account,amount):
    account['balance'] = account['balance'] - amount
    return account['balance']

def my_balance(account):
    return "The balance for {} ".format(account,account['balance'])


## main
milad = Account()
print (milad)
my_deposit(milad,1000)
my_withdraw(milad,200)
print (my_balance(milad))

## sunil
sunil = Account()
print (sunil)
my_deposit(sunil,2000)
my_withdraw(sunil,200)
my_withdraw(sunil,200)
print (my_balance(sunil))
    
