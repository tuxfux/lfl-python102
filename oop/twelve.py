#!/usr/bin/env python
# -*- coding: utf-8 -*-
# requirement1: pass a name as a string. - "santosh kumar"
#@classmethod - a constructor but call be called along with class.
#@staticmethod -> general methods which you bundle into the class.
# https://www.toptal.com/python/python-design-patterns



class Student(object):
    def __init__(self,first_name,last_name):
        self.first_name = first_name
        self.last_name = last_name
        
    @classmethod
    def from_string(cls,name_str):
        first_name,last_name = name_str.split()
        student = cls(first_name,last_name)
        return student
    
    @staticmethod
    def is_full_name(name_str):
        name = name_str.split()
        return len(name) > 1
    
    def student_details(self):
        return ("The student details are - {} {}".format(self.first_name,self.last_name))

## main

student1 = Student("milad","wolverine")
print (student1.student_details())

student2 = Student.from_string("milad superman")
print (student2.student_details())

##
print (Student.is_full_name("milad superman"))
print (Student.is_full_name("milad wolverine"))


