#!/usr/bin/python
# understanding method and accessing variables in method.
# -*- coding: utf-8 -*-

# class/blueprint
# class is a child of object class.
#class Account(object):
class Account:
    balance = 0 # data
    def my_balance(self): #  method
        print ("my balance is {}".format(self.balance))
        
    # self.balance refers to instance data.
    # Account.balance refers to class data.
        
# main
sunil = Account()
## error1
#sunil.my_balance()
#TypeError: my_balance() takes 0 positional arguments but 1 was given
## self refers to the instance.
## error2
#sunil.my_balance()
#NameError: name 'balance' is not defined
## if we call a method via a instance/object(sunil),we can access only the 
## instance/class variables.
sunil.my_balance()  # my balance is 0
sunil.balance = 200
sunil.my_balance() # 0

## milad
milad = Account()
milad.my_balance() # 0