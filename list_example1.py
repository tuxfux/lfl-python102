#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
days = ['yesterday','today','tomorrow','dayafter']

output1:
yesterday 9
today     5
tomorrow  8
dayafter  8

output2:
Yesterday
TOday
TOMorrow
DAYAfter

Notes:
    
In [112]: len?
Signature: len(obj, /)
Docstring: Return the number of items in a container.
Type:      builtin_function_or_method

In [113]: len('today')
Out[113]: 5

""" 


## solution 1:
days = ['yesterday','today','tomorrow','dayafter']
for day in days:
    print ("{} {}".format(day,len(day)))
    
  
'''
### notes
In [116]: days = ['yesterday','today','tomorrow','dayafter']

In [117]: days.index('yesterday')
Out[117]: 0

In [118]: days.index('today')
Out[118]: 1

In [119]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
yesterday 0
today 1
tomorrow 2
dayafter 3

In [120]: 'yesterday'[0]
     ...: 
Out[120]: 'y'

In [121]: 'today'[1]
Out[121]: 'o'

In [122]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
y
o
m
a

In [123]: 'today'[:1]
Out[123]: 't'

In [124]: 'today'[0:1]
Out[124]: 't'

In [125]: 'yesterday'[0]
Out[125]: 'y'

In [126]: 'yesterday'[0:0]
Out[126]: ''

In [127]: 'yesterday'[0:0 + 1]
Out[127]: 'y'

In [128]: 'today'[1]
Out[128]: 'o'

In [129]: 'today'[0:1 + 1]
Out[129]: 'to'

In [130]: 'tomorrow'[0:2 + 1]
Out[130]: 'tom'

In [131]: 'dayafter'[0:3+1]
Out[131]: 'daya'

In [132]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
y
o
m
a

In [133]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
y
to
tom
daya

In [134]: 'y'.upper()
Out[134]: 'Y'


In [136]: 

In [136]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
y
to
tom
daya


In [138]: 

In [138]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
Y
TO
TOM
DAYA

In [139]: "python"
Out[139]: 'python'

In [140]: "python"[0:3]
Out[140]: 'pyt'

In [141]: "python"[:3]
Out[141]: 'pyt'

In [142]: "python"[3:]
Out[142]: 'hon'

In [143]: "python"[:2]
Out[143]: 'py'

In [144]: "python"[2:]
Out[144]: 'thon'

In [145]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
Y
TO
TOM
DAYA

In [146]: runfile('C:/Users/tuxfux/Documents/lfl-python102/list_example1.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')
Yesterday
TOday
TOMorrow
DAYAfter

In [147]: 
    
'''

## solution 2:
## input
days = ['yesterday','today','tomorrow','dayafter']

for day in days:
    #print (day,days.index(day))
    print (day[:days.index(day) + 1].upper() + day[days.index(day) + 1:])



