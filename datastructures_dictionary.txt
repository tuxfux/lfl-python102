# dictionary
# hash/dict

# key and value

#no  name    subject
0001 milad -> python
0002 sunil -> python
0003 milad -> django

no is the key
name,subject the values

ex:
ssn - US
aadhar - India

In [78]: my_exams = {'1':'Milad','2':'sunil','3':'aarav','4':'santosh'}

In [79]: print (my_exams)
{'1': 'Milad', '2': 'sunil', '3': 'aarav', '4': 'santosh'}

In [80]: # dictionaries are key based and nothing to do with position

In [81]: print (type(my_exams))
<class 'dict'>

In [82]: my_empty = dict()

In [83]: print (my_empty,type(my_empty))
{} <class 'dict'>

In [84]: my_empty = {}

In [85]: print (my_empty,type(my_empty))
{} <class 'dict'>

In [86]: 
    
In [87]: # list -> [a,b],[],list()

In [88]: # tuple -> (a,b),(),tuple()

In [89]: # dictionary -> {'a':'apple','b':'ball'},{},dict()

In [90]: 
    
In [91]: my_exams = {'1':'Milad','2':'sunil','3':'aarav','4':'santosh'}

In [92]: my_exams['1']
Out[92]: 'Milad'

In [93]: my_exams['4']
Out[93]: 'santosh'

In [94]: # No indexing and slicing concepts here.

In [95]: # we access the values depeneding on the key

In [96]: ## in

In [97]: # in works only on keys in a dictionary

In [98]: '1' in my_exams
Out[98]: True

In [99]: 'Milad' in my_exams
Out[99]: False

In [100]: 


In [1]: my_exams = {'1':'Milad','2':'sunil','3':'aarav','4':'santosh'}

In [2]: for key in my_exams:
   ...:     print (key,my_exams[key])
   ...:     
1 Milad
2 sunil
3 aarav
4 santosh

In [3]: 

In [4]: my_exams = {'1':'Milad','2':'sunil','3':'aarav','4':'santosh'}

In [5]: # insert a value into the dictionary

In [6]: my_exams['new']='kumar'

In [7]: print (my_exams)
{'1': 'Milad', '2': 'sunil', '3': 'aarav', '4': 'santosh', 'new': 'kumar'}

In [8]: ## replace/update/append

In [9]: my_exams['1']="vikram"

In [10]: print (my_exams)
{'1': 'vikram', '2': 'sunil', '3': 'aarav', '4': 'santosh', 'new': 'kumar'}

In [11]: my_exams['1']=["vikram","milad"]

In [12]: print (my_exams)
{'1': ['vikram', 'milad'], '2': 'sunil', '3': 'aarav', '4': 'santosh', 'new': 'kumar'}


In [14]: ## function

In [15]: my_exams
Out[15]: 
{'1': ['vikram', 'milad'],
 '2': 'sunil',
 '3': 'aarav',
 '4': 'santosh',
 'new': 'kumar'}

In [16]: #my_exams.keys

In [17]: my_exams.keys?
Docstring: D.keys() -> a set-like object providing a view on D's keys
Type:      builtin_function_or_method

In [18]: my_exams.keys()
Out[18]: dict_keys(['1', '2', '3', '4', 'new'])

In [21]: #my_exams.values
    ...: 

In [22]: my_exams.values?
Docstring: D.values() -> an object providing a view on D's values
Type:      builtin_function_or_method

In [23]: my_exams.values()
    ...: 
Out[23]: dict_values([['vikram', 'milad'], 'sunil', 'aarav', 'santosh', 'kumar'])

In [24]: 


In [26]: # more like a in operation

In [27]: my_exams.get?
Signature: my_exams.get(key, default=None, /)
Docstring: Return the value for key if key is in the dictionary, else default.
Type:      builtin_function_or_method

In [28]: my_exams.get('new')
Out[28]: 'kumar'

In [29]: my_exams.get('n')

In [31]: 

In [31]: print (my_exams.get('n'))
None


In [33]: my_exams
Out[33]: 
{'1': ['vikram', 'milad'],
 '2': 'sunil',
 '3': 'aarav',
 '4': 'santosh',
 'new': 'kumar'}

In [34]: my_exams['3']
Out[34]: 'aarav'

In [35]: my_exams.get['3']
Traceback (most recent call last):

In [36]: 

In [36]: my_exams.get('3')
Out[36]: 'aarav'

In [37]:


In [38]: #my_exams.items

In [39]: my_exams.items?
Docstring: D.items() -> a set-like object providing a view on D's items
Type:      builtin_function_or_method

In [40]: my_exams.items()
Out[40]: dict_items([('1', ['vikram', 'milad']), ('2', 'sunil'), ('3', 'aarav'), ('4', 'santosh'), ('new', 'kumar')])

In [41]: 

 In [42]: #my_exams.update

In [43]: my_exams.update?
Docstring:
D.update([E, ]**F) -> None.  Update D from dict/iterable E and F.
If E is present and has a .keys() method, then does:  for k in E: D[k] = E[k]
If E is present and lacks a .keys() method, then does:  for k, v in E: D[k] = v
In either case, this is followed by: for k in F:  D[k] = F[k]
Type:      builtin_function_or_method

In [44]: my_exams
Out[44]: 
{'1': ['vikram', 'milad'],
 '2': 'sunil',
 '3': 'aarav',
 '4': 'santosh',
 'new': 'kumar'}

In [45]: my_new_students = {'5':'bendu','6':'mounika','7':'kishan','8':'phani'}

In [46]: my_exams.update(my_new_students)

In [47]: print (my_exams)
{'1': ['vikram', 'milad'], '2': 'sunil', '3': 'aarav', '4': 'santosh', 'new': 'kumar', '5': 'bendu', '6': 'mounika', '7': 'kishan', '8': 'phani'}


In [49]: ##my_exams.setdefault

In [50]: my_exams.setdefault
Out[50]: <function dict.setdefault(key, default=None, /)>

In [51]: my_exams.setdefault?
Signature: my_exams.setdefault(key, default=None, /)
Docstring:
Insert key with a value of default if key is not in the dictionary.

Return the value for key if key is in the dictionary, else default.
Type:      builtin_function_or_method

In [52]: my_exams = {'1':'Milad','2':'sunil','3':'aarav','4':'santosh'}

In [53]: my_exams.setdefault("5","vikram")
Out[53]: 'vikram'

In [54]: my_exams
Out[54]: {'1': 'Milad', '2': 'sunil', '3': 'aarav', '4': 'santosh', '5': 'vikram'}

In [55]: my_exams.setdefault("1","mounika")
Out[55]: 'Milad'

In [56]: my_exams
Out[56]: {'1': 'Milad', '2': 'sunil', '3': 'aarav', '4': 'santosh', '5': 'vikram'}

In [57]: my_exams.setdefault("6")

In [58]: my_exams
Out[58]: 
{'1': 'Milad',
 '2': 'sunil',
 '3': 'aarav',
 '4': 'santosh',
 '5': 'vikram',
 '6': None}
 
 
 In [60]: ##my_exams.fromkeys

In [61]: my_exams.fromkeys?
Signature: my_exams.fromkeys(iterable, value=None, /)
Docstring: Create a new dictionary with keys from iterable and values set to value.
Type:      builtin_function_or_method

In [62]: dict.fromkeys(my_exams,"numbers")
Out[62]: 
{'1': 'numbers',
 '2': 'numbers',
 '3': 'numbers',
 '4': 'numbers',
 '5': 'numbers',
 '6': 'numbers'}

In [63]: dict.fromkeys(my_exams,"Tutor santosh")
Out[63]: 
{'1': 'Tutor santosh',
 '2': 'Tutor santosh',
 '3': 'Tutor santosh',
 '4': 'Tutor santosh',
 '5': 'Tutor santosh',
 '6': 'Tutor santosh'}

In [64]: my_exams.fromkeys(my_exams,"tutor")
Out[64]: 
{'1': 'tutor',
 '2': 'tutor',
 '3': 'tutor',
 '4': 'tutor',
 '5': 'tutor',
 '6': 'tutor'}

In [65]: 


### my_exams.pop

In [69]: my_exams.pop?
Docstring:
D.pop(k[,d]) -> v, remove specified key and return the corresponding value.
If key is not found, d is returned if given, otherwise KeyError is raised
Type:      builtin_function_or_method

In [70]: my_exams
Out[70]: 
{'1': 'Milad',
 '2': 'sunil',
 '3': 'aarav',
 '4': 'santosh',
 '5': 'vikram',
 '6': None}

In [71]: my_exams.pop('6')

In [72]: print (my_exams)
{'1': 'Milad', '2': 'sunil', '3': 'aarav', '4': 'santosh', '5': 'vikram'}

In [73]: my_exams.pop('6')
Traceback (most recent call last):

  File "<ipython-input-73-445cf2984949>", line 1, in <module>
    my_exams.pop('6')

KeyError: '6'

## my_exams.popitem

In [75]: my_exams.popitem?
Docstring:
D.popitem() -> (k, v), remove and return some (key, value) pair as a
2-tuple; but raise KeyError if D is empty.
Type:      builtin_function_or_method

In [76]: my_exams
Out[76]: {'1': 'Milad', '2': 'sunil', '3': 'aarav', '4': 'santosh', '5': 'vikram'}

In [77]: my_exams.popitem()
Out[77]: ('5', 'vikram')

In [78]: my_exams.popitem()
Out[78]: ('4', 'santosh')

In [79]: my_exams.popitem()
Out[79]: ('3', 'aarav')

In [80]: my_exams
Out[80]: {'1': 'Milad', '2': 'sunil'}

## clear dictionary

In [82]: my_exams.clear?
Docstring: D.clear() -> None.  Remove all items from D.
Type:      builtin_function_or_method

In [83]: my_exams.clear()

In [85]: 

In [85]: my_exams
Out[85]: {}


In [87]: my_exams = {'1':'Milad','2':'sunil','3':'aarav','4':'santosh'}

In [88]: my_exams.copy?
Docstring: D.copy() -> a shallow copy of D
Type:      builtin_function_or_method

In [89]: # soft copy,deep copy and shallow copy

In [90]: # soft copy -> multiple object liking to a similar memory location

In [91]: # deep copy -> each object linking to a different memory location

In [92]: # shallow copy -> complex objects -> inner object have similar memory where as otuer object has different memory locations.

In [93]: 

In [98]: a = [1,2]

In [99]: b = [3,4]

In [100]: id(a),id(b),id(a[0]),id(a[1]),id(b[0]),id(b[1])
Out[100]: 
(2194549980808,
 2194550641864,
 140724132942656,
 140724132942688,
 140724132942720,
 140724132942752)

In [101]: print (id(a),id(b),id(a[0]),id(a[1]),id(b[0]),id(b[1]))
2194549980808 2194550641864 140724132942656 140724132942688 140724132942720 140724132942752

In [102]: Fco = [a,b]

In [103]: print (Fco)
[[1, 2], [3, 4]]

In [104]: print (id(Fco),id(Fco[0]),id(Fco[1]))
2194550533384 2194549980808 2194550641864

In [105]: 

In [108]: ## soft copy

In [109]: Sco = Fco

In [110]: print (id(Fco),id(Fco[0]),id(Fco[1]))
2194550533384 2194549980808 2194550641864

In [111]: print (id(Sco),id(Sco[0]),id(Sco[1]))
2194550533384 2194549980808 2194550641864

In [113]: 

In [113]: Fco
Out[113]: [[1, 2], [3, 4]]

In [114]: Sco
Out[114]: [[1, 2], [3, 4]]

In [115]: Sco[0] = [11,22]

In [116]: # the change in Sco should be reflected in Fco

In [117]: Fco
Out[117]: [[11, 22], [3, 4]]

In [118]: Sco
Out[118]: [[11, 22], [3, 4]]

In [119]: 

In [119]: ## deep copy

In [120]: Dco = copy.deepcopy(Fco)

In [121]: print (id(Fco),id(Fco[0]),id(Fco[1]))
2194550533384 2194550643656 2194550641864

In [122]: print (id(Dco),id(Dco[0]),id(Dco[1]))
2194550543496 2194537732680 2194537733832

In [123]: Dco
Out[123]: [[11, 22], [3, 4]]

In [124]: Fco
Out[124]: [[11, 22], [3, 4]]

In [125]: Dco[0]=[55,66]

In [126]: Dco
Out[126]: [[55, 66], [3, 4]]

In [127]: Fco
Out[127]: [[11, 22], [3, 4]]

In [128]: 

## shallow copy

In [129]: Fco
Out[129]: [[11, 22], [3, 4]]

In [130]: print (id(Fco),id(Fco[0]),id(Fco[1]))
2194550533384 2194550643656 2194550641864

In [131]: import copy

In [132]: copy.copy?
Signature: copy.copy(x)
Docstring:
Shallow copy operation on arbitrary Python objects.

See the module's __doc__ string for more info.
File:      c:\users\tuxfux\anaconda3\lib\copy.py
Type:      function

In [133]: Sco = copy.copy(Fco)

In [134]: print (Sco)
[[11, 22], [3, 4]]

In [135]: print (id(Fco),id(Fco[0]),id(Fco[1]))
2194550533384 2194550643656 2194550641864

In [136]: print (id(Sco),id(Sco[0]),id(Sco[1]))
2194550730376 2194550643656 2194550641864

In [137]: print (Fco)
[[11, 22], [3, 4]]

In [138]: print (Sco)
[[11, 22], [3, 4]]

In [139]: Fco[0]
Out[139]: [11, 22]

In [140]: Fco[0][0]="eleven"

In [141]: print (Fco)
[['eleven', 22], [3, 4]]

In [142]: print (Sco)
[['eleven', 22], [3, 4]]

In [143]: Fco[0] = ["eleven","twelve"]

In [144]: print (Fco)
[['eleven', 'twelve'], [3, 4]]

In [145]: print (Sco)
[['eleven', 22], [3, 4]]

real use case:

As a whole the complex entity is a different memory location.
the internal objects are still liked to the similar memory block.
## hardcopy,softcopy -> shallow copy
[[python,django,AWS],[list of costing]] 
[[python,django,AWS],[timing]]

## exercises - assignment
1. create a dictionary and try to print in the order,it
was entered. 2.x and 3.x
2. create a dictionary and find out the key for a specific
value.

my_exams = {'1':'Milad','2':'sunil','3':'aarav','4':'santosh'}

if i provide the name 'Milad' -> key is 1

# https://www.coursera.org/specializations/python
# Learning python the hardway 
  + google.com -> + freepdf


