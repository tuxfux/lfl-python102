#!/usr/bin/python
## this has to be run in a linux box.
import logging
from subprocess import Popen,PIPE
import shlex
import re

## Advanced logging methodologies
logger = logging.getLogger('Disk stats') # logger
logger.setLevel(logging.DEBUG)               # filter at logger level
ch = logging.FileHandler('myApp.txt')                # handler
ch.setLevel(logging.DEBUG)                  # filter at handler level.
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter) # handler and formatter.
logger.addHandler(ch)      # logger and handler

# to find the disk space of /
# df -h / | tail -n 1 | awk '{print $5}' | sed -e 's#%##g'
# disk_size = $(df -h / | tail -n 1 | awk '{print $5}' | sed -e 's#%##g')
#disk_size = int(input("please enter the disk size:"))
p1 = Popen(shlex.split("df -h /"),stdout=PIPE)
p2 = Popen(shlex.split("tail -n 1"),stdin=p1.stdout,stdout=PIPE)
output = p2.communicate()[0]
disk_size = int(re.search('([0-9]+)%',output).group(1))

## disk_size is good
if disk_size < 50:
    logger.info("My disk is healty at {}".format(disk_size))
elif disk_size < 80:
    logger.warning("The disk is getting filled up {}".format(disk_size))
elif disk_size < 90:
    logger.error("The disk is almost full {}".format(disk_size))
elif disk_size <= 100:
    logger.critical("The Application has gone down {}".format(disk_size))
    
## run the code once
## later create a file of size 5gb
##dd if=/dev/zero of=bigfile.txt bs=1M count=5000
## df -h /
## du -sh bigfile.txt
    