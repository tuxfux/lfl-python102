#!/usr/bin/python
## please run it in a linux box only.
import logging as l
from subprocess import Popen,PIPE
import shlex
import re

l.basicConfig(filename="myApp.txt",
              filemode="a",
              level=l.DEBUG,
              format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
              datefmt='%c')

# to find the disk space of /
# df -h / | tail -n 1 | awk '{print $5}' | sed -e 's#%##g'
# disk_size = $(df -h / | tail -n 1 | awk '{print $5}' | sed -e 's#%##g')
#disk_size = int(input("please enter the disk size:"))
p1 = Popen(shlex.split("df -h /"),stdout=PIPE)
p2 = Popen(shlex.split("tail -n 1"),stdin=p1.stdout,stdout=PIPE)
output = p2.communicate()[0]
disk_size = int(re.search('([0-9]+)%',output).group(1))

## disk_size is good
if disk_size < 50:
    l.info("My disk is healty at {}".format(disk_size))
elif disk_size < 80:
    l.warning("The disk is getting filled up {}".format(disk_size))
elif disk_size < 90:
    l.error("The disk is almost full {}".format(disk_size))
elif disk_size <= 100:
    l.critical("The Application has gone down {}".format(disk_size))
    
## run the code once
## later create a file of size 5gb
##dd if=/dev/zero of=bigfile.txt bs=1M count=5000
## df -h /
## du -sh bigfile.txt
    