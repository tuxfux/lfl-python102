#!/usr/bin/python
import logging

# set up logging to file - see previous section for more details
#logger is root
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='myapp.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()  # handler
console.setLevel(logging.INFO)     # filter for handler.
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)  # format for handler
# add the handler to the root logger
logging.getLogger('').addHandler(console) # message going from logging/root logger
                                          # should go back to console too.

# Now, we can log to the root logger, or any other logger. First the root...
logging.info('Jackdaws love my big sphinx of quartz.')

# Now, define a couple of other loggers which might represent areas in your
# application:

# we have two loggers.
logger1 = logging.getLogger('myapp.area1')
logger2 = logging.getLogger('myapp.area2')

## there is no handler/formatter or any relation set between logger/handler/foramtter

## logger messages
logger1.debug('Quick zephyrs blow, vexing daft Jim.')
logger1.info('How quickly daft jumping zebras vex.')
logger2.warning('Jail zesty vixen who grabbed pay from quack.')
logger2.error('The five boxing wizards jump quickly.')