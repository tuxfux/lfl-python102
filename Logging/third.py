#!/usr/bin/python
import logging as l

l.basicConfig(filename="myApp.txt",
              filemode="a",
              level=l.DEBUG,
              format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
              datefmt='%c')

# to find the disk space of /
# this is a manual method
disk_size = int(input("please enter the disk size:"))

if disk_size < 50:
    l.info("My disk is healty at {}".format(disk_size))
elif disk_size < 80:
    l.warning("The disk is getting filled up {}".format(disk_size))
elif disk_size < 90:
    l.error("The disk is almost full {}".format(disk_size))
elif disk_size <= 100:
    l.critical("The Application has gone down {}".format(disk_size))
    