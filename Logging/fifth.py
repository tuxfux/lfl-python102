#!/usr/bin/python
#Loggers expose the interface that application code directly uses.
## for logging: root is the default logger
## we can create our own loggers.
#Handlers send the log records (created by loggers) to the appropriate destination.
## ex: filename="myApp.txt",filemode="a" (filehandler)
## reference: https://docs.python.org/3/howto/logging.html#useful-handlers
#Filters provide a finer grained facility for determining which log records to output.
## ex: level=l.DEBUG -> handler level
## we can set filters at both  logger level and handler level.
#Formatters specify the layout of log records in the final output.
## format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
## datefmt='%c'

## in a given program.
## we can have multiple loggers
## we can have multiple handlers
## we can have multiple filters
## we can have multiple formatters

#l.basicConfig(filename="myApp.txt",
#              filemode="a",
#              level=l.DEBUG,
#              format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
#              datefmt='%c')


import logging

# create logger
## todo : __name__ should reflect the file name
logger = logging.getLogger('simple example') # logger
logger.setLevel(logging.DEBUG)               # filter at logger level

# debug < info < warning < error < critical

# create console handler and set level to debug
## logging module provide FileHandler and StreamHandler
## logging.handlers provide us other handlers details
ch = logging.StreamHandler()                # handler
ch.setLevel(logging.DEBUG)                  # filter at handler level.

# debug < info < warning < error < critical

# create formatter
## what is the format of our message.
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

## how to intelink the logger,handler and formatter
# add formatter to ch
ch.setFormatter(formatter) # handler and formatter.

# add ch to logger
logger.addHandler(ch)      # logger and handler

# 'application' code
logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
logger.critical('critical message')