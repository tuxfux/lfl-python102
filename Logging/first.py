#!/usr/bin/python
# -*- coding: utf-8 -*-
# by default warning is the default level.
#The default level is WARNING, which means that only events of 
#this level and above will be tracked, 
#unless the logging package is configured to do otherwise.

import logging as l
l.debug("This is a debug message")
l.info("This is a information message")
l.warning("This is a warning message")
l.error("This is a error message")
l.critical("This is a critical message")


#(base) C:\Users\tuxfux\Documents\lfl-python102\Logging>python first.py
#WARNING:root:This is a warning message
#ERROR:root:This is a error message
#CRITICAL:root:This is a critical message