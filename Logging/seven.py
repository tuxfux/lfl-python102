#!/usr/bin/python
import logging

logger = logging.getLogger('simple_example')
logger.setLevel(logging.ERROR)
# create file handler which logs even debug messages
fh = logging.FileHandler('spam.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

# 'application' code
logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
logger.critical('critical message')

##
## friends trying to reach you.
## father - logger - head of the house
##  filters -  everyone except your friends are allowed into the house. - your friends are not allowed.
## kumar  - handler - my room
## filters - everyone except my friends are not allowed into the house - your friends are allowed into your room.