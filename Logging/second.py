#!/usr/bin/python
# -*- coding: utf-8 -*-
# by default warning is the default level.
#The default level is WARNING, which means that only events of 
#this level and above will be tracked, 
#unless the logging package is configured to do otherwise.

# import logging
# logging.basicConfig?
# logging.Formatter?
# datefmt -> man date or google.com -> man date manual + unix

import logging as l

l.basicConfig(filename="mylog.txt",
              filemode="a",
              level=l.DEBUG,
              format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
              datefmt='%c')

l.debug("This is a debug message")
l.info("This is a information message")
l.warning("This is a warning message")
l.error("This is a error message")
l.critical("This is a critical message")

#challenges:
#* basic config can redirect logs to file 
# handles are the location where the logfiles are redirected - files.
# we cannot give a name to our logger.By default its root.
# this is only for small teams.(5)