# Functions
# block of code within your program, which you can call multiple times.

+ program1
Fun A
{
  block A
}

# Main

--
-
-
-
FunA
-
-
-
FunA

```python
## Function funA has no parameters
# funA is the defination of the function.
def funA():   
    print ("hello world")
```


```python
## call the function
## every function has a return value.
# if a function doesnt have a return value,it provides us None.

print (funA())
```

    hello world
    None
    


```python
## Function funA has no parameters
# we have a return values so we dont get None.
# return is not a print statement. It marks the end of the function.
# it will pass the control flow from funA to the main part of the program
def funA():   
    return ("hello world")
    print ("Hi guys")
    print ("What are you up to")
    print ("What will you guys do")
```


```python
## MAIN
print (funA())
```

    hello world
    


```python
## variable scope/namespace
# local variables and global variables
```


```python
## local variables
## variable defined inside a function are called local variables/namespaces/scope.
## The are available during the run time of the function.
# The lifespan of hte local variable is during the runtime of the function.
# local variable cannot be called in main.
# locals() -> pre defined function - which will show us local scope/namespace.
```


```python
def funB():
    print (locals())
    a = 10
    print (locals())
    return (a)
```


```python
# Main
print (funB())
```

    {}
    {'a': 10}
    10
    


```python
print (a)

```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-15-cb9bacd097d9> in <module>
    ----> 1 print (a)
    

    NameError: name 'a' is not defined



```python
print (locals())
```

    {'__name__': '__main__', '__doc__': 'Automatically created module for IPython interactive environment', '__package__': None, '__loader__': None, '__spec__': None, '__builtin__': <module 'builtins' (built-in)>, '__builtins__': <module 'builtins' (built-in)>, '_ih': ['', '## Function funA has no parameters\ndef funA():   \n    print "hello world"', '## Function funA has no parameters\ndef funA():   \n    print ("hello world")', '## call the function\nprint (FunA())', '## call the function\nprint (funA())', '## Function funA has no parameters\ndef funA():   \n    return ("hello world")', 'print (funA())', '## MAIN\nprint (funA())', 'def funB():\n    a = 10\n    return (a)', '# Main\nprint (funB())', 'print (a)', 'def funB():\n    print (locals())\n    a = 10\n    print (locals())\n    return (a)', '# Main\nprint (funB())', 'print (a)\nprint locals()', 'print (a)\nprint (locals())', 'print (a)', 'print (locals())'], '_oh': {}, '_dh': ['C:\\Users\\tuxfux\\Documents\\lfl-python102\\function'], 'In': ['', '## Function funA has no parameters\ndef funA():   \n    print "hello world"', '## Function funA has no parameters\ndef funA():   \n    print ("hello world")', '## call the function\nprint (FunA())', '## call the function\nprint (funA())', '## Function funA has no parameters\ndef funA():   \n    return ("hello world")', 'print (funA())', '## MAIN\nprint (funA())', 'def funB():\n    a = 10\n    return (a)', '# Main\nprint (funB())', 'print (a)', 'def funB():\n    print (locals())\n    a = 10\n    print (locals())\n    return (a)', '# Main\nprint (funB())', 'print (a)\nprint locals()', 'print (a)\nprint (locals())', 'print (a)', 'print (locals())'], 'Out': {}, 'get_ipython': <bound method InteractiveShell.get_ipython of <ipykernel.zmqshell.ZMQInteractiveShell object at 0x000001E4BC748AC8>>, 'exit': <IPython.core.autocall.ZMQExitAutocall object at 0x000001E4BC7A7F98>, 'quit': <IPython.core.autocall.ZMQExitAutocall object at 0x000001E4BC7A7F98>, '_': '', '__': '', '___': '', '_i': 'print (a)', '_ii': 'print (a)\nprint (locals())', '_iii': 'print (a)\nprint locals()', '_i1': '## Function funA has no parameters\ndef funA():   \n    print "hello world"', '_i2': '## Function funA has no parameters\ndef funA():   \n    print ("hello world")', 'funA': <function funA at 0x000001E4BC9519D8>, '_i3': '## call the function\nprint (FunA())', '_i4': '## call the function\nprint (funA())', '_i5': '## Function funA has no parameters\ndef funA():   \n    return ("hello world")', '_i6': 'print (funA())', '_i7': '## MAIN\nprint (funA())', '_i8': 'def funB():\n    a = 10\n    return (a)', 'funB': <function funB at 0x000001E4BC951950>, '_i9': '# Main\nprint (funB())', '_i10': 'print (a)', '_i11': 'def funB():\n    print (locals())\n    a = 10\n    print (locals())\n    return (a)', '_i12': '# Main\nprint (funB())', '_i13': 'print (a)\nprint locals()', '_i14': 'print (a)\nprint (locals())', '_i15': 'print (a)', '_i16': 'print (locals())'}
    


```python
## global variables
## variable available to me both in local and global namespaces.
# global variables can be accessed from within a function
# if given my global variable is not accessible, we get an exception.
# globals() is a function to show you global variables
```


```python
## x is a global variable , its defined in global scope.
## funC is a function.
x = 10
def funC():
    print (locals())
    return (x)
def funD():
    print (locals())
    return (y)
```


```python
print (funC())
```

    {}
    10
    


```python
print (funD())
```

    {}
    


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-33-e0a8601a1a9c> in <module>
    ----> 1 print (funD())
    

    <ipython-input-31-fbccca72646c> in funD()
          7 def funD():
          8     print (locals())
    ----> 9     return (y)
    

    NameError: name 'y' is not defined

globals() is a function to show global variables.

In [4]: globals()
Out[4]:
{'__name__': '__main__',
 '__doc__': 'Automatically created module for IPython interactive environment',
 '__package__': None,
 '__loader__': None,
 '__spec__': None,
 '__builtin__': <module 'builtins' (built-in)>,
 '__builtins__': <module 'builtins' (built-in)>,
 '_ih': ['',
  'globals()_',
  'clear',
  "get_ipython().run_line_magic('cls', '')",
  'globals()'],
 '_oh': {},
 '_dh': ['C:\\Users\\tuxfux'],
 'In': ['',
  'globals()_',
  'clear',
  "get_ipython().run_line_magic('cls', '')",
  'globals()'],
 'Out': {},
 'get_ipython': <bound method InteractiveShell.get_ipython of <IPython.terminal.interactiveshell.TerminalInteractiveShell object at 0x000001DC9A9D02E8>>,
 'exit': <IPython.core.autocall.ExitAutocall at 0x1dc9bb42f60>,
 'quit': <IPython.core.autocall.ExitAutocall at 0x1dc9bb42f60>,
 '_': '',
 '__': '',
 '___': '',
 '_i': 'cls',
 '_ii': 'clear',
 '_iii': 'globals()_',
 '_i1': 'globals()_',
 '_i2': 'clear',
 '_i3': 'cls',
 '_i4': 'globals()'}

In [5]: 

## globals() always shows you global variables, even if you call from a within a function.
## locals() which shows local variables inside a function and show global variables outside a function.

```python
# global keyworlds
```


```python
amount = 0

def deposit():
    amount = amount + 1000
    return amount
```


```python
print (deposit())
```


    ---------------------------------------------------------------------------

    UnboundLocalError                         Traceback (most recent call last)

    <ipython-input-6-c78eb9baafbc> in <module>
    ----> 1 print (deposit())
    

    <ipython-input-5-8e24e58cdacf> in deposit()
          2 
          3 def deposit():
    ----> 4     amount = amount + 1000
          5     return amount
    

    UnboundLocalError: local variable 'amount' referenced before assignment



```python
def deposit():
    amount = 0
    amount = amount + 1000
    return amount
def withdraw():
    amount = 0
    amount = amount - 200
    return amount
```


```python
print (deposit())
```

    1000
    


```python
print (withdraw())
```

    -200
    


```python
## global
amount = 0

def deposit():
    global amount
    print (locals())
    amount = amount + 1000
    return amount
def withdraw():
    global amount
    print (locals())
    amount = amount - 200
    return amount
```


```python
print (deposit())
```

    {}
    1000
    


```python
print (deposit())
```

    {}
    2000
    


```python
print (amount)
```

    2000
    


```python
print (withdraw())
```

    {}
    1800
    


```python
## Functional arguments
```


```python
# a,b are called functinal parameters/arguments
def my_add(a,b):
    print (locals())
    return a + b
```


```python
## positional based arguments
print (my_add(11,22))
```

    {'a': 11, 'b': 22}
    33
    


```python
print (my_add("python"," rocks"))
```

    {'a': 'python', 'b': ' rocks'}
    python rocks
    


```python
print (my_add(" rocks","python"))
```

    {'a': ' rocks', 'b': 'python'}
     rockspython
    


```python
## keyword based arguments
print (my_add(b=" rocks",a="python"))
```

    {'a': 'python', 'b': ' rocks'}
    python rocks
    


```python
## default arguments
```


```python
## example: i want to go for multiplication of 2
# max_value is the default functional argument in our case.
```


```python
def my_multi(num,max_value=10):
    for value in range(1,max_value + 1):
        print ("{} * {} = {}".format(num,value,num*value))
```


```python
my_multi(2)
```

    2 * 1 = 2
    2 * 2 = 4
    2 * 3 = 6
    2 * 4 = 8
    2 * 5 = 10
    2 * 6 = 12
    2 * 7 = 14
    2 * 8 = 16
    2 * 9 = 18
    2 * 10 = 20
    


```python
my_multi(2,5)
```

    2 * 1 = 2
    2 * 2 = 4
    2 * 3 = 6
    2 * 4 = 8
    2 * 5 = 10
    


```python
my_multi(max_value=5,num=4)
```

    4 * 1 = 4
    4 * 2 = 8
    4 * 3 = 12
    4 * 4 = 16
    4 * 5 = 20
    


```python
## putty
## https://simple.wikipedia.org/wiki/PuTTY#/media/File:PuTTY.PNG
```


```python
def putty(hostname,port=22):
    pass
## putty(abc.com) # port-22
## putty(abc.com,23) # port-23
## putty(port=23,hostname=abc.com) # port-23
```


```python
## *,**,*args,**kwargs
```


```python
# *
```


```python
def my_add(a,b):
    return a + b
```


```python
my_list = [11,22]
my_new_list = [11,22,33]
## one of the ways
# a = my_list[0]
# b = my_list[1]
# my_add(a,b)
```


```python
my_add(*my_list)
```




    33




```python
my_add(my_list)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-6-5746e55ec0d6> in <module>
    ----> 1 my_add(my_list)
    

    TypeError: my_add() missing 1 required positional argument: 'b'



```python
my_add(*my_new_list)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-7-b74890102ab7> in <module>
    ----> 1 my_add(*my_new_list)
    

    TypeError: my_add() takes 2 positional arguments but 3 were given



```python
# **
```


```python
def my_add(a,b):
    return a + b
```


```python
my_dict = {'b':'keep a docter away.','a':'a apple a day '}
my_new_dict = {'a':'a apple a day ','c':'keep a docter away.'}
# this is one of the ways
# a = my_dict['a']
# b = my_dict['b']
# my_add(a,b)
```


```python
my_add(**my_dict)
```




    'a apple a day keep a docter away.'




```python
my_add(my_dict)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-13-6e060dbe398b> in <module>
    ----> 1 my_add(my_dict)
    

    TypeError: my_add() missing 1 required positional argument: 'b'



```python
my_add(**my_new_dict)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-16-8b124e6c8e8a> in <module>
    ----> 1 my_add(**my_new_dict)
    

    TypeError: my_add() got an unexpected keyword argument 'c'



```python
## args
# think a scenerio where number of arguments are more.
```


```python
# system defined function.
# inbuilt into python
help(max)
```

    Help on built-in function max in module builtins:
    
    max(...)
        max(iterable, *[, default=obj, key=func]) -> value
        max(arg1, arg2, *args, *[, key=func]) -> value
        
        With a single iterable argument, return its biggest item. The
        default keyword-only argument specifies an object to return if
        the provided iterable is empty.
        With two or more arguments, return the largest argument.
    
    


```python
max(22,33)
```




    33




```python
max(22,33,55,44,11)
```




    55




```python
max(-5,-3,-4,-9,-1,-6,-8)
```




    -1


# gmax
# user defined function.
# args -> tuple of your functional arguments
def gmax(*args):
    return (args)

```python
def gmax(*args):
    big = -1
    for value in args:
        if value > big:
            big = value
    return (big)
```


```python
gmax(22,33)
```




    33




```python
gmax(22,33,55,44,11)
```




    55




```python
gmax(-5,-3,-4,-9,-1,-6,-8)
```




    -1




```python
## **kwargs
```
# call center
# guya -> call center.
# call center asks few questions
1.  # name,address
2.  # name,mother maiden,address
3.  # name,emailid,mother maiden,address

```python
# kwargs -> provides us a dictionary
def callme(**kwargs):
    return (kwargs)
```


```python
def callme(**kwargs):
    if 'name' in kwargs:
        print (kwargs['name'])
    if 'address' in kwargs:
        print (kwargs['address'])
    if 'mother' in kwargs:
        print (kwargs['mother'])
    if 'email' in kwargs:
        print (kwargs['email'])
```


```python
def callme(**kwargs):
    for key in kwargs.keys():
        print ("The value for {} is {}".format(key,kwargs[key]))
```


```python
callme(name="milad",address="dallas")
```

    The value for name is milad
    The value for address is dallas
    


```python
callme(name="milad",mother='neena',address="dallas")
```

    The value for name is milad
    The value for mother is neena
    The value for address is dallas
    


```python
callme(name="milad",email="milad.python@gmail.com",mother='neena',address="dallas")
```

    The value for name is milad
    The value for email is milad.python@gmail.com
    The value for mother is neena
    The value for address is dallas
    
## we can user both *args and **kwargs in our functions

```python
def my_add(*args,**kwargs):
    if args:
        return args
    if kwargs:
        return kwargs
```


```python
my_add(2,3)
```




    (2, 3)




```python
my_add(a=2,b=3)
```




    {'a': 2, 'b': 3}




```python
# Assignment - fibanocci series in python
# Assignment - Factorial of a number
# call function within a function.
```


```python
## map,filter and lambda
```


```python
#3.x
help(map)
```

    Help on class map in module builtins:
    
    class map(object)
     |  map(func, *iterables) --> map object
     |  
     |  Make an iterator that computes the function using arguments from
     |  each of the iterables.  Stops when the shortest iterable is exhausted.
     |  
     |  Methods defined here:
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __next__(self, /)
     |      Implement next(self).
     |  
     |  __reduce__(...)
     |      Return state information for pickling.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
    
    
# 2.x
In [1]: map?
Docstring:
map(function, sequence[, sequence, ...]) -> list

Return a list of the results of applying the function to the items of
the argument sequence(s).  If more than one sequence is given, the
function is called with an argument list consisting of the corresponding
item of each sequence, substituting None for missing values when not all
sequences have the same length.  If the function is None, return a list of
the items of the sequence (or a list of tuples if more than one sequence).
Type:      builtin_function_or_method

In [2]:

```python
def square(a):
    return (a*a)
```


```python
print (square(2))
```

    4
    


```python
print (square(4))
```

    16
    


```python
# 3.x
map(square,range(1,20,2))
```




    <map at 0x2181bfb0390>




```python
for value in map(square,range(1,20,2)):
    print (value)
```

    1
    9
    25
    49
    81
    121
    169
    225
    289
    361
    


```python
[ value for value in map(square,range(1,20,2))]
```




    [1, 9, 25, 49, 81, 121, 169, 225, 289, 361]


# 2.x
In [3]: def square(a):
   ...:     return a*a
   ...:

In [4]: map(square,range(1,20,2))
Out[4]: [1, 9, 25, 49, 81, 121, 169, 225, 289, 361]

In [5]: range(1,20,2)
Out[5]: [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]


```python
# filter
```


```python
help(filter)
```

    Help on class filter in module builtins:
    
    class filter(object)
     |  filter(function or None, iterable) --> filter object
     |  
     |  Return an iterator yielding those items of iterable for which function(item)
     |  is true. If function is None, return the items that are true.
     |  
     |  Methods defined here:
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __next__(self, /)
     |      Implement next(self).
     |  
     |  __reduce__(...)
     |      Return state information for pickling.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
    
    
# 2.x
In [6]: filter?
Docstring:
filter(function or None, sequence) -> list, tuple, or string

Return those items of sequence for which function(item) is true.  If
function is None, return the items that are true.  If sequence is a tuple
or string, return the same type, else return a list.
Type:      builtin_function_or_method


```python
# If a function provides a value its called as True function for a given value.
# if a function provides None its called as False function for a given value.
def even(a):
    if a % 2 == 0:
        return 'even'
```


```python
even(2) # for value 2 the function is True
```




    'even'




```python
print (even(3)) # for value 3 the function is not True/False
```

    None
    
# 2.x


In [6]: range(1,11)
Out[6]: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

In [7]: def even(a):
   ...:     if a % 2 == 0:
   ...:         return 'even'
   ...:

In [8]: filter(even,range(1,11))
Out[8]: [2, 4, 6, 8, 10]


```python
# 3.x
# range(1,11) -> numbers from 1 to 10
filter(even,range(1,11))
```




    <filter at 0x2181bfc6390>




```python
# list comprehension
[ value for value in filter(even,range(1,11))]
```




    [2, 4, 6, 8, 10]




```python
# assignment
```


```python
def square(a):
    return (a*a)
```


```python
# map on square
[ value for value in map(square,range(1,20,2))]
```




    [1, 9, 25, 49, 81, 121, 169, 225, 289, 361]




```python
# filter on square
# range(1,20,2) -> 1,3,5,7,9,11,13,15,17,19
[ value for value in filter(square,range(1,20,2))]
```




    [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]




```python
def even(a):
    if a % 2 == 0:
        return 'even'
```


```python
# filter
[ value for value in filter(even,range(1,11))]
```




    [2, 4, 6, 8, 10]




```python
# map
# range(1,11) -> 1,2,3,4,5,6,7,8,9,10
[ value for value in map(even,range(1,11))]
```




    [None, 'even', None, 'even', None, 'even', None, 'even', None, 'even']




```python
## lambda - nameless function
## works along with map and filter.
# using lambda taken me away from defining functions which are very small.
```


```python
# map on square
[ value for value in map(square,range(1,20,2))]
# the output which is got is a intermediate value.
```




    [1, 9, 25, 49, 81, 121, 169, 225, 289, 361]




```python
# lamdba with map on a nameless square function.
[value for value in map(lambda a:a*a,range(1,20,2))]
```




    [1, 9, 25, 49, 81, 121, 169, 225, 289, 361]




```python
# lamdba with filter on a nameless even function
[ value for value in filter(lambda a:a%2==0,range(1,11))]
```




    [2, 4, 6, 8, 10]


## Assignement
https://github.com/zhiwehu/Python-programming-exercises/blob/master/100%2B%20Python%20challenging%20programming%20exercises.txt

```python

```
