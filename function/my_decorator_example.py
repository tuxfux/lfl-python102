#!/usr/bin/python
# -*- coding: utf-8 -*-

## decorator

def topwrapper(func):
    def downwrapper(*args,**kwargs):
        try:
            func(*args,**kwargs)
        except Exception as e:
            return ("My exception is - {}".format(e))
        else:
            return func(*args,**kwargs)
    return downwrapper

## functions
@topwrapper
def my_add(a,b):
    return a + b

@topwrapper
def my_div(a,b):
    return a/b

@topwrapper
def my_multi(a,b):
    return a * b

@topwrapper
def my_sub(a,b):
    if a > b:
        return a - b
    else:
        return b - a

print (my_add(1,2))
print (my_add("linux",11))

print (my_div(4,2))
print (my_div(4,0))

print (my_sub(11,'a'))


## old way
#my_add = topwrapper(my_add)
#print (my_add)
#print (my_add(1,3))
#print (my_add("linux",1))
#print (my_add(a=1,b=2))
#print (my_add(a="linux",b=1))






## problem

#def my_add(a,b):
#    try:
#        a + b
#    except Exception as e:
#        return ("My exception is - {}".format(e))
#    else:
#        return a + b
#    
#def my_div(a,b):
#    try:
#        a/b
#    except Exception as e:
#        return ("My exception is - {}".format(e))
#    else:
#        return (a/b)
#
## Main
#print (my_add(1,2))
#print (my_add("linux",11))
#
#print (my_div(4,2))
#print (my_div(4,0))


