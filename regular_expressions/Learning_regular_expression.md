# what is regular expression/pattern matching/regex

```python
import re
```


```python
print (dir(re))
```

    ['A', 'ASCII', 'DEBUG', 'DOTALL', 'I', 'IGNORECASE', 'L', 'LOCALE', 'M', 'MULTILINE', 'Match', 'Pattern', 'RegexFlag', 'S', 'Scanner', 'T', 'TEMPLATE', 'U', 'UNICODE', 'VERBOSE', 'X', '_MAXCACHE', '__all__', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '__version__', '_cache', '_compile', '_compile_repl', '_expand', '_locale', '_pickle', '_special_chars_map', '_subx', 'compile', 'copyreg', 'enum', 'error', 'escape', 'findall', 'finditer', 'fullmatch', 'functools', 'match', 'purge', 'search', 'split', 'sre_compile', 'sre_parse', 'sub', 'subn', 'template']
    


```python
# example

answer = input("Do you want to come to a movie? ")
if answer == 'yes':
    print ("You are welcome to the movie.")
else:
    print ("You are not welcome to the movie.")
```

    Do you want to come to a movie? Yes
    You are not welcome to the move.
    


```python
# re.match
help(re.match)
```

    Help on function match in module re:
    
    match(pattern, string, flags=0)
        Try to apply the pattern at the start of the string, returning
        a Match object, or None if no match was found.
    
    


```python
# patter is a subset of the string
# string="python"
# subset -> p,py,pyt,pyth,python
# ! subset -> pythons,sno,th
```
# 2.x
In [1]: import re

In [2]: my_string="python"

In [3]: re.match("py",my_string)
Out[3]: <_sre.SRE_Match at 0x415d510>


```python
# 3.x
my_string = "python"
re.match("py",my_string)
```




    <re.Match object; span=(0, 2), match='py'>




```python
re.match("Py",my_string,re.IGNORECASE)
```




    <re.Match object; span=(0, 2), match='py'>




```python
re.match("Py",my_string,re.I)
```




    <re.Match object; span=(0, 2), match='py'>




```python
# example
# combination - yes - 8
import re

answer = input("Do you want to come to a movie(yes/no)? ")
#if answer == 'yes':
if re.match(answer,'yes',re.I):   # true if there is a parttern match object
    print ("You are welcome to the movie.")
else:
    print ("You are not welcome to the movie.")
```

    Do you want to come to a movie(yes/no)? Yes
    You are welcome to the movie.
    


```python
# re.search
```


```python
help(re.search)
```

    Help on function search in module re:
    
    search(pattern, string, flags=0)
        Scan through string looking for a match to the pattern, returning
        a Match object, or None if no match was found.
    
    


```python
my_string="linux"
print (re.search('in',my_string))
print (re.search('nux',my_string))
print (re.search('Li',my_string,re.I))
```

    <re.Match object; span=(1, 3), match='in'>
    <re.Match object; span=(2, 5), match='nux'>
    <re.Match object; span=(0, 2), match='li'>
    


```python
## special characters
# ^ - caret -> Search/match from the beginning of a string/sentence/line.
# $ - dollar -> Search/match at the end of a string/sentence/line.
# . - dot    -> search/match for a character in a string/sentence/line.
```


```python
my_sentence = "Today is saturday"
my_sentence1 = "its saturday today"

## match
print (re.match("today",my_sentence,re.I)) # <re.Match object; span=(0, 5), match='Today'>
print (re.match("today",my_sentence1,re.I)) # None 

## search - ^
print (re.search("today",my_sentence1,re.I)) # <re.Match object; span=(13, 18), match='today'>
print (re.search("^today",my_sentence1,re.I)) # None

## search - $
print (re.search("saturday",my_sentence)) # <re.Match object; span=(9, 17), match='saturday'>
print (re.search("saturday",my_sentence1)) # <re.Match object; span=(4, 12), match='saturday'>
print (re.search("saturday$",my_sentence)) # <re.Match object; span=(9, 17), match='saturday'>
print (re.search("saturday$",my_sentence1)) # None

## search - .
print (re.search('.....',my_sentence)) # <re.Match object; span=(0, 5), match='Today'>
print (re.search('^.....',my_sentence)) # <re.Match object; span=(0, 5), match='Today'>
print (re.search('.....$',my_sentence)) # <re.Match object; span=(12, 17), match='urday'>
```

    <re.Match object; span=(0, 5), match='Today'>
    None
    <re.Match object; span=(13, 18), match='today'>
    None
    <re.Match object; span=(9, 17), match='saturday'>
    <re.Match object; span=(4, 12), match='saturday'>
    <re.Match object; span=(9, 17), match='saturday'>
    None
    <re.Match object; span=(0, 5), match='Today'>
    <re.Match object; span=(0, 5), match='Today'>
    <re.Match object; span=(12, 17), match='urday'>
    


```python
# example
# we have to allow friend who name lenght is 5 characters.

import re
my_friends = ['sunil','milad','santosh','aarav','venkatesh','chiranjeevi','nagarjuna']

for value in my_friends:
    if re.search('^.....$',value):
        print (re.search('^.....$',value))
        print (re.search('^.....$',value).group())
```

    <re.Match object; span=(0, 5), match='sunil'>
    sunil
    <re.Match object; span=(0, 5), match='milad'>
    milad
    <re.Match object; span=(0, 5), match='aarav'>
    aarav
    


```python
## dotall
# . never matches \n or \r
my_string = "python\n"
print (my_string)
print (re.match('.......',my_string))
print (re.match('.......',my_string,re.DOTALL))
print (re.match('.......',my_string,re.DOTALL).group())
```

    python
    
    None
    <re.Match object; span=(0, 7), match='python\n'>
    python
    
    


```python
## globbling characters
## * -> matches zero or more characters.
## + -> matches one or more characters.
## ? -> matches zero or one characters.
```


```python
# re.compile
help(re.compile)
```

    Help on function compile in module re:
    
    compile(pattern, flags=0)
        Compile a regular expression pattern, returning a Pattern object.
    
    


```python
my_film = "ashique"
my_film1 = "aashique"
my_film2 = "aaashique"
my_film3 = "shique"
```


```python
reg = re.compile('a*shique',re.I) # a*shique -> a repeated zero or more times.
print (reg)
print (dir(reg))
```

    re.compile('a*shique', re.IGNORECASE)
    ['__class__', '__copy__', '__deepcopy__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'findall', 'finditer', 'flags', 'fullmatch', 'groupindex', 'groups', 'match', 'pattern', 'scanner', 'search', 'split', 'sub', 'subn']
    


```python
# reg.match
help(re.match)

```

    Help on function match in module re:
    
    match(pattern, string, flags=0)
        Try to apply the pattern at the start of the string, returning
        a Match object, or None if no match was found.
    
    


```python
# reg.match
help(reg.match)
```

    Help on built-in function match:
    
    match(string, pos=0, endpos=9223372036854775807) method of re.Pattern instance
        Matches zero or more characters at the beginning of the string.
    
    
my_film = "ashique"
my_film1 = "aashique"
my_film2 = "aaashique"
my_film3 = "shique"

```python
# our pattern was taken care in reg - ('a*shique', re.IGNORECASE)
print (reg.match(my_film))
# re.match('a*shique', my_film,re.IGNORECASE)
print (reg.match(my_film1))
# re.match('a*shique', my_film1,re.IGNORECASE)
print (reg.match(my_film2))
# re.match('a*shique', my_film2,re.IGNORECASE)
print (reg.match(my_film3))
# re.match('a*shique', my_film2,re.IGNORECASE)

```

    <re.Match object; span=(0, 7), match='ashique'>
    <re.Match object; span=(0, 8), match='aashique'>
    <re.Match object; span=(0, 9), match='aaashique'>
    <re.Match object; span=(0, 6), match='shique'>
    
my_film = "ashique"
my_film1 = "aashique"
my_film2 = "aaashique"
my_film3 = "shique"

```python
# + 
reg1 = re.compile('a+shique',re.I)# our pattern was taken care in reg - ('a+shique', re.IGNORECASE)
print (reg1.match(my_film))
print (reg1.match(my_film1))
print (reg1.match(my_film2))
print (reg1.match(my_film3))

```

    <re.Match object; span=(0, 7), match='ashique'>
    <re.Match object; span=(0, 8), match='aashique'>
    <re.Match object; span=(0, 9), match='aaashique'>
    None
    


```python
my_film = "ashique"
my_film1 = "aashique"
my_film2 = "aaashique"
my_film3 = "shique"
```


```python
# ? - matches a patter zero or one time
reg2 = re.compile('a?shique',re.I)# our pattern was taken care in reg - ('a?shique', re.IGNORECASE)
# ashique,shique
print (reg2.search(my_film))
print (reg2.search(my_film1))
print (reg2.search(my_film2))
print (reg2.search(my_film3))
```

    <re.Match object; span=(0, 7), match='ashique'>
    <re.Match object; span=(1, 8), match='ashique'>
    <re.Match object; span=(2, 9), match='ashique'>
    <re.Match object; span=(0, 6), match='shique'>
    


```python
# ashique,shique
print (reg2.match(my_film))
print (reg2.match(my_film1))
print (reg2.match(my_film2))
print (reg2.match(my_film3))
```

    <re.Match object; span=(0, 7), match='ashique'>
    None
    None
    <re.Match object; span=(0, 6), match='shique'>
    


```python
# globbling - greedy characters - *,+,? always go for maximal matching
# minimal matching - *?,+?,??
my_string = "<h1>how is it going on</h1>"


import re
# maximial matching
print (re.search('<.*>',my_string))
# minimal matching
print (re.search('<.*?>',my_string))
print (re.search('<.*?>',my_string).group())
```

    <re.Match object; span=(0, 27), match='<h1>how is it going on</h1>'>
    <re.Match object; span=(0, 4), match='<h1>'>
    <h1>
    
# Anchors
{m} -> Match any characters m times.
{m,} -> Match any characater m or more number of times.
{m,n} -> Match any character between m or n number of time.

```python
my_film = "ashique"
my_film1 = "aashique"
my_film2 = "aaashique"
my_film3 = "shique"
```


```python
import re
reg3 = re.compile('a{2,}shique')
print (reg3.match(my_film)) # false
print (reg3.match(my_film1)) # True
print (reg3.match(my_film2)) # True
print (reg3.match(my_film3)) # False
```

    None
    <re.Match object; span=(0, 8), match='aashique'>
    <re.Match object; span=(0, 9), match='aaashique'>
    None
    


```python
# Character sets
```
[a-zA-Z] -> Any character between a to z or A - Z.
[0-9] -> Any character between 0 to 9.
[^a-z] -> Any character not starting with a to z.
[^0-9] -> Any character not starting with 0 to 9.
^[0-9] -> Any character starting with 0 to 9.
^[a-zA-Z] -> Any character staring with a to z.
[.*+?] -> Any special characters in character set are treated as symbols.

```python
my_sentence = "Is today a tuesday?"

# other notes
## \s -> single space,\s+ represents multiple spaces.
import re
print (re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z?]+',my_sentence,re.I).group())
#print (re.match('[a-zA-Z]+\s+[a-z]+\s+[a-z]+\s+[a-z?]+',my_sentence).group())
```

    Is today a tuesday?
    


```python
#print (re.match('\w+\s+\w+\s+\w+\s+\w+[?]',my_sentence,re.I).group())
print (re.match('(\w+\s+){3}\w+[?]',my_sentence,re.I).group())
```

    Is today a tuesday?
    


```python
## re.VERBOSE or re.X 
## | -> pipe
print (re.match('''
(\w+\s+)        # A word and a space
{3}             # Total of three words having (word and space ) including the above.
\w+[?]          # A word followed by ?
''',my_sentence,re.I|re.X).group())
```

    Is today a tuesday?
    


```python
# grouping
# group() -> works on a matched pattern
```


```python
my_sentence = "Is today a tuesday?"
import re
print (re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z?]+',my_sentence,re.I))  # True
print (re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z?]+',my_sentence,re.I).group())

## i want two things - today tuesday
print (re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z]+)[?]',my_sentence,re.I).group(0)) # nothing changed.

# grouping - using positions
print (re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z]+)[?]',my_sentence,re.I).group(1)) # today
print (re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z]+)[?]',my_sentence,re.I).group(2)) # tuesday
print (re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z]+)[?]',my_sentence,re.I).group(1,2)) # ('today','tuesday')
print (re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z]+)[?]',my_sentence,re.I).groups())  # ('today','tuesday')

# grouping - Keybased positions 
# (?P<key>)
print (re.match('[a-z]+\s+(?P<wd>[a-z]+)\s+[a-z]+\s+(?P<tu>[a-z]+)[?]',my_sentence,re.I).group(0))

# grouping
print (re.match('[a-z]+\s+(?P<wd>[a-z]+)\s+[a-z]+\s+(?P<tu>[a-z]+)[?]',my_sentence,re.I).group('wd'))
print (re.match('[a-z]+\s+(?P<wd>[a-z]+)\s+[a-z]+\s+(?P<tu>[a-z]+)[?]',my_sentence,re.I).group('tu'))
print (re.match('[a-z]+\s+(?P<wd>[a-z]+)\s+[a-z]+\s+(?P<tu>[a-z]+)[?]',my_sentence,re.I).group('wd','tu'))
print (re.match('[a-z]+\s+(?P<wd>[a-z]+)\s+[a-z]+\s+(?P<tu>[a-z]+)[?]',my_sentence,re.I).groups())
```

    <re.Match object; span=(0, 19), match='Is today a tuesday?'>
    Is today a tuesday?
    Is today a tuesday?
    today
    tuesday
    ('today', 'tuesday')
    ('today', 'tuesday')
    Is today a tuesday?
    today
    tuesday
    ('today', 'tuesday')
    ('today', 'tuesday')
    


```python
## multilines
# re.MULTILINES,re.M
```


```python
my_love = "Python is my love.\nPython is a great language.\nPython is very easy to approach."
```


```python
my_love
```




    'Python is my love.\nPython is a great language.\nPython is very easy to approach.'




```python
print (my_love)
```

    Python is my love.
    Python is a great language.
    Python is very easy to approach.
    


```python
# python 
print (re.match('python',my_love,re.I)) # true
print (re.search('python',my_love,re.I)) # true

```

    <re.Match object; span=(0, 6), match='Python'>
    <re.Match object; span=(0, 6), match='Python'>
    


```python
print (help(re.findall))
print (re.findall('python',my_love,re.I)) # all occurances
```

    Help on function findall in module re:
    
    findall(pattern, string, flags=0)
        Return a list of all non-overlapping matches in the string.
        
        If one or more capturing groups are present in the pattern, return
        a list of groups; this will be a list of tuples if the pattern
        has more than one group.
        
        Empty matches are included in the result.
    
    None
    ['Python', 'Python', 'Python']
    


```python
## findall on my_love for pattern beginning with python
# mulitline
print (my_love)
```

    Python is my love.
    Python is a great language.
    Python is very easy to approach.
    


```python
# single line
my_love
```




    'Python is my love.\nPython is a great language.\nPython is very easy to approach.'




```python
re.findall('^python',my_love,re.I)
```




    ['Python']




```python
re.findall('^python',my_love,re.I|re.M)
```




    ['Python', 'Python', 'Python']




```python

```
reference
https://docs.python.org/3.7/howto/regex.html
https://docs.python.org/3.7/library/re.html#regular-expression-examples