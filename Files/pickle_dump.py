#!/usr/bin/python
# -*- coding: utf-8 -*-

## pickling
import pickle
my_students = ['milad','sunil','aarav','kumar','santosh']


with open('students_data.txt','ab') as studat:
    pickle.dump(my_students,studat)
    
'''
* we can save our object into a file.
* we can retrive that data from any where.

-- challenges
* There cannot be a cluster of data dumped into file.

'''
    

