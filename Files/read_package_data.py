#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
reg = re.compile("(\w+\s+){2}(?P<package>[a-z0-9-:.+]+)",re.I)

with open('package_info.txt','r') as read_file:
    for line in read_file:
        if re.search("setting",line,re.I):
            if reg.match(line):
                print (reg.match(line).group('package'))

