#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 08:38:59 2019

@author: tuxfux
"""
## unpickling
import pickle
with open("students_data.txt","rb") as read_data:
    my_student = pickle.load(read_data)
    
print (my_student)

