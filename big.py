#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 07:40:22 2019

@author: tuxfux

and -> if both conditions satify then its a True.
or  -> if one of the conditions satify then its a True.

"""

num1 = int(input("pleaes enter the num1:"))
num2 = int(input("please enter the num2:"))
num3 = int(input("pleaes enter the num3:"))

if num1 > num2 and num1 > num3:
    print ("{0} is greater than {1} and {2}".format(num1,num2,num3))
elif num2 > num3 and num2 > num1:
    print ("{1} is greater than {0} and {2}".format(num1,num2,num3))
elif num3 > num1 and num3 > num2:
    print ("{2} is greater than {0} and {1}".format(num1,num2,num3))
else:
    print ("All numbers are equal - {},{},{}".format(num1,num2,num3))    


'''
# Assignment
In [50]: runfile('C:/Users/tuxfux/Documents/lfl-python102/big.py', wdir='C:/Users/tuxfux/Documents/lfl-python102')

pleaes enter the num1:10

please enter the num2:10

pleaes enter the num3:5
All numbers are equal - 10,10,5


'''
