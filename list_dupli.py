#!/usr/bin/python
# -*- coding: utf-8 -*-
# list function in memory

# input
#my_fruits = ['apple','apple','banana','banana','cherry']
my_fruits = ['apple','banana','apple','apple','banana','banana','cherry']
#my_fruits_copy = my_fruits  # soft copy -> wont help
#my_fruits_copy = copy.deepcopy(my_fruits) # deep copy -> better
#output
# my_fruits = ['apple','banana','cherry']
# my_dupli = ['apple','banana']

my_dupli = list()

for fruit in my_fruits[:]:
    if my_fruits.count(fruit) > 1:
        if fruit not in my_dupli:
            my_dupli.append(fruit)
        my_fruits.remove(fruit)
        
print ("my_fruits:{}".format(my_fruits))
print ("my_dupli:{}".format(my_dupli))

