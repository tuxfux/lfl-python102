#!/usr/bin/env python
# -*- coding: utf-8 -*-

#import pdb
version = 3.0

def my_add(a,b):
    ''' Addition of two numbers '''
    a = int(a)
    b = int(b)
    c = a + b
    return c

def my_sub(a,b):
    ''' This is for substraction of two numbers '''
    if a > b:
        return (a - b)
    else:
        return (b - a)
    
def my_multi(a,b):
    ''' This is for multiplication of two numbers '''
    return (a * b)

def my_div(a,b):
    ''' This is for division of two numbers '''
    return (a/b)

## Main
if __name__ == '__main__':
    print ("Welcome to the debugging sessions")
    print ("We are going to jump into pdb prompt")
    #pdb.set_trace()
    print ("Addition of two numbers is {}".format(my_add(11,22)))
    print ("Substraction of two numbers is {}".format(my_sub(44,32)))
    print ("Multiplication of two numbers is {}".format(my_multi(2,5)))
    print ("Division of two numbers is {}".format(my_div(4,2)))