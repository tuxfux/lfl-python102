python:

2.x(2020) and 3.x(future)

## Linux
 # Ubuntu/zorion/linuxmint

 # sudo apt-get install python  (2.x)
 # sudo apt-get install python3 (3.x)
 ## interpreters
 # sudo apt-get install ipython (2.x)
 # sudo apt-get install ipython3 (3.x)
 # sudo apt-get install bpython (2.x)
 # sudo apt-get install bpython3 (3.x)

https://ipython.org/
https://bpython-interpreter.org/

## windows
https://www.anaconda.com/distribution/#windows

IDE
---
http://bamanzi.github.io/scrapbook/data/20140220220052/
pycharm,sublime text,komodo,Scite,spyder

Parent site:
https://www.python.org/

-- tuxfux.hlp@gmail.com -- 