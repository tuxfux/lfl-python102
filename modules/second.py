#!/usr/bin/python 
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0,'C:\\Users\\tuxfux\\Documents\\lfl-python102\\modules\\extra')
print (sys.path)

import first as f

def my_add(a,b):
    ''' This is for addition of two string '''
    a = str(a)
    b = str(b)
    c = a + b
    return (c)

if __name__ == '__main__':
    print ("Addition of two string is {}".format(my_add("linux","rocks")))
    print ("Addition of two numbers is {}".format(f.my_add(22,33)))
    
"""
Modules share function between multiple program without worrying about
each others name space.

Usecase I : i have to add both paths in sys.path location
second.py
 extra
  first.py
 extra1
  third.py

Usecase II: packages
second.py
  extra
    first.py(f1)
  extra1
    first.py(f2)

"""

