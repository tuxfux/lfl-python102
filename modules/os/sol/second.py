#!/usr/bin/python
# -*- coding: utf-8 -*-

def my_sol2_first():
    ''' This is  my first sol function '''
    return "This is my first sol2 function"

def my_sol2_second():
    ''' This is my second sol function '''
    return "This is my second sol2 function"

def my_sol2_third():
    ''' This is my third sol function '''
    return "This is my third sol2 function"

def my_sol2_fourth():
    ''' This is my fourth sol function '''
    return "This is my fourth sol2 function "


