#!/usr/bin/python
# -*- coding: utf-8 -*-

def my_sol1_first():
    ''' This is  my first sol function '''
    return ("This is my first sol1 function")

def my_sol1_second():
    ''' This is my second sol function '''
    return ("This is my second sol1 function")

def my_sol1_third():
    ''' This is my third sol function '''
    return ("This is my third sol1 function")

def my_sol1_fourth():
    ''' This is my fourth sol function '''
    return ("This is my fourth sol1 function ")

