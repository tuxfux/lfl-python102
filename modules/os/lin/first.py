#!/usr/bin/python
# -*- coding: utf-8 -*-

def my_lin1_first():
    ''' This is  my first lin function '''
    return "This is my first lin1 function"

def my_lin1_second():
    ''' This is my second lin function '''
    return "This is my second lin1 function"

def my_lin1_third():
    ''' This is my third lin function '''
    return "This is my third lin1 function"

def my_lin1_fourth():
    ''' This is my fourth lin function '''
    return "This is my fourth lin1 function "

